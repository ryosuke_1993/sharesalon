class CreateRooms < ActiveRecord::Migration[6.1]
  def change
    create_table :rooms do |t|
      t.string :title
      t.text :body
      t.string :room_image_id
      t.integer :price
      t.string :address

      t.timestamps
    end
  end
end
