class CreateRequests < ActiveRecord::Migration[6.1]
  def change
    create_table :requests do |t|
      t.references :user, null: false, foreign_key: true
      t.references :room, foreign_key: true
      t.string :customer_name
      t.string :email
      t.string :phone_number
      t.datetime :start_date
      t.datetime :end_date
      t.text :message

      t.timestamps
    end
  end
end
