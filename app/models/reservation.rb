class Reservation < ApplicationRecord
  belongs_to :user
  belongs_to :room
  validate :date_before
  validate :date_before_start
  validate :not_tuesday
  validates_time :start_date, between: ['9:00am', '10:00pm']
  validates :start_date, uniqueness: { message: 'は他のユーザーが予約しています' }
  validates :start_date, presence: true
  validates :end_date, presence: true
  validate :same_date

  def date_before
    errors.add(:start_date, "は過去の日付を選択できません") if start_date < Date.today
  end

  def not_tuesday
    errors.add(:start_date, "は火曜日を選択できません") if start_date.tuesday?
  end

  def date_before_start
    errors.add(:end_date, "は開始時以前の日付を選択できません") if end_date <= start_date
  end

  def same_date
    if (start_date.mon != end_date.mon) || (start_date.mday != end_date.mday)
      errors.add(:end_date, "日をまたいでの予約はできません")
    end
  end

  def start_time
    start_date
  end
end
