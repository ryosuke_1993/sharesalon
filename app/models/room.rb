class Room < ApplicationRecord
  attachment :room_image
  has_many :reservations, dependent: :destroy
  has_many :requests, dependent: :destroy
  geocoded_by :address
  after_validation :geocode, if: :address_changed?
  validates :title, presence: true, uniqueness: true
  validates :body, presence: true
  validates :address, presence: true
  validates :price, presence: true
end
