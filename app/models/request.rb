class Request < ApplicationRecord
  belongs_to :user
  belongs_to :room
  validate :date_before_start
  validate :date_before
  validate :not_tuesday
  validates_time :start_date, between: ['9:00am', '10:00pm']
  validates :start_date, uniqueness: { message: ':予約開始日は他のユーザーが予約しています' }
  validates :customer_name, presence: true
  validates :email, presence: true
  validates :phone_number, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true

  def date_before
    errors.add(:start_date, ":予約開始日は過去の日付を選択できません") if start_date < Date.today
  end

  def date_before_start
    errors.add(:end_date, ":予約終了時は開始時以前の日付を選択できません") if end_date <= start_date
  end

  def not_tuesday
    errors.add(:start_date, ":予約開始日は火曜日を選択できません") if start_date.tuesday?
  end

  def start_time
    start_date
  end
end
