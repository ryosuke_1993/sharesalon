class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  attachment :profile_image
  has_many :reservations, dependent: :destroy
  has_many :requests, dependent: :destroy
  validates :username, presence: true
  validates :email, presence: true, uniqueness: true
  def self.guest
    find_or_create_by!(email: 'guest01@example.com', username: 'guest', admin: true) do |user|
      user.password = SecureRandom.urlsafe_base64
    end
  end
end
