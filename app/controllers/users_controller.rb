class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
    @reservations = Reservation.where(user_id: params[:id])
    @rooms = Room.all
    @requests = Request.where(user_id: params[:id])
  end

  def new
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    @user.update(user_params)
    if @user.save && current_user.admin == true
      redirect_to admin_users_path
    elsif @user.save
      redirect_to user_path(@user), notice: "更新が完了しました"
    else
      flash[:danger] = @user.errors.full_messages
      redirect_to edit_user_path(@user)
    end
  end

  def destroy
    reset_session
    redirect_to root_path, notice: 'ログアウトしました。'
  end

  private

  def user_params
    params.require(:user).permit(:username, :email, :profile, :profile_image, :admin)
  end
end
