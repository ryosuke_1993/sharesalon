class Admin::RoomsController < ApplicationController
  before_action :admin_user

  def index
    @rooms = Room.all
  end

  def new
    @room = Room.new
  end

  def create
    @room = Room.new(room_params)
    if @room.save
      redirect_to room_path(@room), notice: "更新しました"
    else
      flash[:danger] = @room.errors.full_messages
      redirect_to new_admin_room_path
    end
  end

  def show
    @room = Room.find(params[:id])
  end

  def edit
    @room = Room.find(params[:id])
  end

  def update
    @room = Room.find(params[:id])
    @room.update(room_params)
    redirect_to room_path(@room)
  end

  def destroy
    room = Room.find(params[:id])
    if current_user.admin?
      room.destroy
      redirect_to admin_rooms_path, notice: '部屋を削除しました。'
    end
  end

  private

  def room_params
    params.require(:room).permit(:title, :body, :room_image, :price, :address)
  end

  def admin_user
    redirect_to(root_path) unless current_user.admin?
  end
end
