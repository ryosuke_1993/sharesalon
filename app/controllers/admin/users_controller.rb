class Admin::UsersController < ApplicationController
  before_action :admin_user

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
  end

  def destroy
    user = User.find(params[:id])
    if current_user.admin?
      user.destroy
      redirect_to admin_users_path, notice: 'ユーザーを削除しました。'
    end
  end

  private

  def admin_user
    redirect_to(root_path) unless current_user.admin?
  end
end
