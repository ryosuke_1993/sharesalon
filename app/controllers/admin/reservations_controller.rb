class Admin::ReservationsController < ApplicationController
  before_action :admin_user

  def index
    @reservations = Reservation.all.order(start_date: "DESC")
    @rooms = Room.all
    @users = User.all
  end

  def destroy
    reservation = Reservation.find(params[:id])
    if current_user.admin?
      reservation.destroy
      redirect_to admin_reservations_path, notice: '予約を削除しました。'
    end
  end

  def admin_user
    redirect_to(root_path) unless current_user.admin?
  end
end
