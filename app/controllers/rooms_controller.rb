class RoomsController < ApplicationController
  def index
    @rooms = Room.all
  end

  def new
    @room = Room.new
  end

  def create
    @room = Room.new(room_params)
    if @room.save
      redirect_to room_path(@room), notice: "更新しました"
    else
      flash[:danger] = @room.errors.full_messages
      @room = Room.new(room_params)
      redirect_to new_admin_room_path
    end
  end

  def destroy
    room = Room.find(params[:id])
    if current_user.admin?
      room.destroy
      redirect_to admin_rooms_path, notice: '部屋を削除しました。'
    end
  end

  def update
    @room = Room.find(params[:id])
    @room.update(room_params)
    if @room.save
      redirect_to room_path(@room), notice: "更新しました"
    else
      flash[:danger] = @room.errors.full_messages
      redirect_to edit_admin_room_path(@room)
    end
  end

  def show
    @room = Room.find(params[:id])
    gon.room = @room
    @reservations = Reservation.where(room_id: params[:id])
  end

  def edit
  end

  private

  def room_params
    params.require(:room).permit(:title, :body, :room_image, :price, :address)
  end
end
