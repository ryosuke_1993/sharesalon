class RequestsController < ApplicationController
  def new
    @request = Request.new
    @rooms = Room.all
    @user = User.find(params[:user_id])
  end

  def index
  end

  def show
  end

  def create
    @user = User.find(params[:user_id])
    @rooms = Room.all
    @request = Request.new(request_params)
    if @request.save
      redirect_to user_path(@user), notice: "リクエストが完了しました"
    else
      flash[:danger] = @request.errors.full_messages
      @request = Request.new(request_params)
      render 'new'
    end
  end

  private

  def request_params
    params.require(:request).permit(:start_date, :end_date, :room_id, :user_id, :customer_name, :email, :phone_number, :message)
  end
end
