class ReservationsController < ApplicationController
  before_action :authenticate_user!
  def index
    @reservations = Reservation.all
  end

  def new
    @room = Room.find(params[:room_id])
    @reservation = Reservation.new
    @reservations = Reservation.where(room_id: @room.id)
  end

  def create
    @room = Room.find(params[:room_id])
    @reservations = Reservation.where(room_id: @room.id)
    @reservation = Reservation.new(reservation_params)
    if @reservation.save
      session[:reservation] = nil
      redirect_to root_path, notice: "予約が完了しました"
    else
      flash[:danger] = @reservation.errors.full_messages
      @reservation = Reservation.new(reservation_params)
      render 'new'
    end
  end

  def edit
  end

  def show
  end

  private

  def reservation_params
    params.require(:reservation).permit(:start_date, :end_date, :room_id, :user_id)
  end
end
