require "test_helper"

class RoomControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get room_index_url
    assert_response :success
  end

  test "should get show" do
    get room_show_url
    assert_response :success
  end

  test "should get edit" do
    get room_edit_url
    assert_response :success
  end
end
