# README

## SaloShare
レンタルサロンのオーナーとサロンを借りたい人、サロンを借りたい人とサロンを利用したい人をつなげるアプリケーションです。  
URL:https://saloshare.herokuapp.com/  
![Scheme](app/assets/images/SaloShare_top_page.png)  

### 概要  
レンタルサロンアプリです。
管理者（オーナー),セラピスト（施術者),エンドユーザー（被施術者)の３タイプのユーザーに分かれます。
レンタルサロンとして貸し出したい部屋を登録し、サロンを借りたい人が時間を指定し(30分ごと)部屋を予約、また、施術を受けたい人はリクエストとして時間、部屋を指定したものをセラピストに送信できます。
ゲストアカウントには管理者機能も付随しています。

### 使用技術  

・Ruby 2.6.3  
・Ruby on Rails 6.1.3  
・MySQL 14.14  
・Puma  
・AWS  
　S3  
・Docker/Docker-compose  
・Circle Ci  
・RSpec  
・Rubocop-airbnb  
・Google Maps API  

### Circle CI  
Bitbucketへpush時に自動でRSpecとRobocopが実行されます。

### 機能一覧
・ユーザー登録、ログイン機能(devise)  
・adminユーザー機能  
　・ユーザーの管理（確認、管理者権限の編集、削除)  
　・ルームの管理（確認、新規作成、編集、削除)  
　・予約の確認  
・ルーム投稿機能  
　・画像投稿(refile)  
　・位置情報検索機能(geocoder)  
　・予約済み表示機能(simple calendar)  
・予約機能(simple calendar)  
・未登録ユーザーからのリクエスト機能  

### テスト
・RSpec  
　・modelテスト  
　・requestテスト  
　・featureテスト  
・Robocop  
