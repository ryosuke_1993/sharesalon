FactoryBot.define do
  factory :reservation do
    start_date { "2022-04-16 12:00:00" }
    end_date { "2022-04-16 13:00:00" }
    association :user
    association :room
  end
end
