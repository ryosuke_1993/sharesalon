FactoryBot.define do
  factory :room do
    sequence(:title) { |n| "MyString#{n}" }
    body { "MyText" }
    room_image_id { "MyString" }
    price { 1 }
    address { "MyString" }
  end
end
