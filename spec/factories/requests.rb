FactoryBot.define do
  factory :request do
    customer_name { "MyString" }
    email { "MyString" }
    phone_number { "MyString" }
    start_date { "2022-04-16 12:00:00" }
    end_date { "2022-04-16 13:00:00" }
    message { "MyText" }
  end
end
