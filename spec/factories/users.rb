FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "MyString#{n}" }
    sequence(:email) { |n| "MyText#{n}@text" }
    password { 12345678 }
  end
end
