require 'rails_helper'

RSpec.feature "Users", type: :feature do
  scenario "View mypage" do
    user = FactoryBot.create(:user, profile: "test")
    room = FactoryBot.create(:room)
    reservation = FactoryBot.create(:reservation, user_id: user.id, room_id: room.id)
    request = FactoryBot.create(:request, user_id: user.id, room_id: room.id)

    travel_to Date.new(2022, 4, 14)

    visit root_path
    click_link "Login"
    fill_in "Email", with: user.email
    fill_in "Password", with: user.password
    click_button "Log in"
    click_link "mypage"

    expect(page).to have_current_path user_path(user.id)
    expect(page).to have_selector ".therapestDetaile h3", text: user.username
    expect(page).to have_selector ".therapestProfile", text: user.profile
    expect(page).to have_link '編集', href: edit_user_path(user.id)
    expect(page).to have_selector ".reservation_room_name span", text: room.title
    expect(page).to have_selector ".reservation_room_date span", text: "#{reservation.start_date.strftime("%m月%d日%H時%M分")}-#{reservation.end_date.strftime("%H時%M分")}"
    expect(page).to have_selector ".requestRoom", text: "希望部屋名: #{room.title}"
    expect(page).to have_selector ".requestDate", text: "#{request.start_date.strftime("%m月%d日%H時%M分")}-#{request.end_date.strftime("%H時%M分")}"
    expect(page).to have_selector ".requestEmail", text: "email: #{request.email}"
    expect(page).to have_selector ".requestTel", text: "TEL: #{request.phone_number}"
  end
end
