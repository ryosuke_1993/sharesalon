require 'rails_helper'

RSpec.feature "Admins", type: :feature do
  scenario "View admin/room index" do
    user = FactoryBot.create(:user)
    user2 = FactoryBot.create(:user, admin: true)
    room = FactoryBot.create(:room)
    room2 = FactoryBot.create(:room)
    reservation = FactoryBot.create(:reservation, user_id: user.id, room_id: room.id)
    travel_to Date.new(2022, 4, 14)

    visit root_path
    click_link "Login"
    fill_in "Email", with: user2.email
    fill_in "Password", with: user2.password
    click_button "Log in"
    click_link "管理画面"
    click_link "ルーム管理画面"

    expect(page).to have_current_path admin_rooms_path
    expect(page).to have_selector ".admin_rooms th", text: room.title
    expect(page).to have_selector ".admin_rooms th", text: room2.title
    expect(page).to have_selector ".admin_rooms th", text: room.price
    expect(page).to have_selector ".admin_rooms th", text: room2.price
    expect(page).to have_selector ".admin_rooms th", text: room.address
    expect(page).to have_selector ".admin_rooms th", text: room2.address
    expect(page).not_to have_selector ".admin_rooms th", text: reservation.start_date
    expect(page).to have_link '編集', href: edit_admin_room_path(room)
    expect(page).to have_link "削除", href: admin_room_path(room)
    expect(page).to have_link "新規作成", href: new_admin_room_path
  end

  scenario "View admin/reservations index" do
    user = FactoryBot.create(:user)
    user2 = FactoryBot.create(:user, admin: true)
    room = FactoryBot.create(:room)
    room2 = FactoryBot.create(:room)
    reservation = FactoryBot.create(:reservation, user_id: user.id, room_id: room.id)
    travel_to Date.new(2022, 4, 14)

    visit root_path
    click_link "Login"
    fill_in "Email", with: user2.email
    fill_in "Password", with: user2.password
    click_button "Log in"
    click_link "管理画面"
    click_link "予約管理画面"

    expect(page).to have_current_path admin_reservations_path
    expect(page).to have_selector ".admin_reservations th", text: user.username
    expect(page).not_to have_selector ".admin_reservations th", text: user2.username
    expect(page).to have_selector ".admin_reservations th", text: room.title
    expect(page).not_to have_selector ".admin_reservations th", text: room2.title
    expect(page).to have_selector ".admin_reservations th", text: user.email
    expect(page).not_to have_selector ".admin_reservations th", text: user2.email
    expect(page).to have_selector ".admin_reservations th", text: "#{reservation.start_date.strftime("%m月%d日%H時%M分")}-#{reservation.end_date.strftime("%H時%M分")}"
    expect(page).not_to have_selector ".admin_rooms th", text: reservation.start_date
    expect(page).to have_link "削除", href: admin_reservation_path(reservation)
  end

  scenario "View admin/users index" do
    user = FactoryBot.create(:user, admin: true)
    travel_to Date.new(2022, 4, 14)

    visit root_path
    click_link "Login"
    fill_in "Email", with: user.email
    fill_in "Password", with: user.password
    click_button "Log in"
    click_link "管理画面"
    click_link "ユーザー管理画面"

    expect(page).to have_current_path admin_users_path
    expect(page).to have_selector ".admin_users th", text: user.username
    expect(page).to have_selector ".admin_users th", text: user.email
    expect(page).to have_selector ".admin_users th", text: true
    expect(page).to have_link "編集", href: edit_admin_user_path(user)
    expect(page).to have_link "削除", href: admin_user_path(user)
  end
end
