require 'rails_helper'

RSpec.feature "Room", type: :feature do
  scenario "View show page" do
    user = FactoryBot.create(:user)
    room = FactoryBot.create(:room)
    reservation = FactoryBot.create(:reservation, user_id: user.id, room_id: room.id)
    travel_to Date.new(2022, 4, 14)
    visit room_path(room.id)

    expect(page).to have_current_path room_path(room.id)
    expect(page).to have_selector "h1", text: room.title
    expect(page).to have_selector ".room_title h3", text: room.title
    expect(page).to have_selector ".room_address p", text: room.address
    expect(page).to have_selector ".room_price p", text: room.price
    expect(page).to have_selector ".room_body p", text: room.body
    expect(page).to have_link '予約', href: new_room_reservation_path(room.id)
    expect(page).to have_selector ".room_body p", text: room.body
    expect(page).to have_selector ".calendar_checkin", text: reservation.start_date.strftime('%H:%M')
    expect(page).to have_selector ".calendar_checkout", text: reservation.end_date.strftime('%H:%M')
  end
end
