require 'rails_helper'

RSpec.describe "Users", type: :request do
  include Devise::Test::IntegrationHelpers
  describe "#show" do
    let(:user) { create(:user, username: "rspec", password: "123456", email: "r@spec.com", profile: "hogehoge", admin: true) }

    before do
      sign_in user
      get user_path(user.id)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "has the user name" do
      expect(response.body).to include user.username
    end

    it "has the profile" do
      expect(response.body).to include user.profile
    end

    it "has the correct header" do
      expect(response.body).to include "Logout"
      expect(response.body).to include "admin"
    end
  end
end
