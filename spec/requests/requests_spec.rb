require 'rails_helper'

RSpec.describe "Requests", type: :request do
  describe "#new" do
    let(:user) { create(:user, username: "rspec", password: "123456", email: "r@spec.com", profile: "hogehoge", admin: true) }

    it "returns http success" do
      get new_user_request_path(user.id)
      expect(response).to have_http_status(:success)
    end
  end
end
