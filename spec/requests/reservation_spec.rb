require 'rails_helper'

RSpec.describe "Reservations", type: :request do
  include Devise::Test::IntegrationHelpers
  describe "#new" do
    let(:user) { create(:user, username: "rspec", password: "123456", email: "r@spec.com", profile: "hogehoge", admin: true) }
    let(:room) { create(:room, title: "yamagoya", body: "nanimonaiyamagoyadesu", price: 2000, address: "yamanashi", room_image_id: 1) }

    before do
      sign_in user
      get new_room_reservation_path(room.id)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "has the user name" do
      expect(response.body).to include room.title
    end

    it "has the correct header" do
      expect(response.body).to include "Logout"
      expect(response.body).to include "admin"
    end
  end
end
