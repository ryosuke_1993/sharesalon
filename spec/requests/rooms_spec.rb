require 'rails_helper'

RSpec.describe "Rooms", type: :request do
  describe "#show" do
    let(:room) { create(:room, title: "rspec", body: "hogehoge", price: 1234, address: "rspec.com", room_image_id: "hogehoge") }

    before do
      get room_path(room.id)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "has the room name" do
      expect(response.body).to include room.title
    end

    it "has the profile" do
      expect(response.body).to include room.body
    end

    it "has the address" do
      expect(response.body).to include room.address
    end
  end
end
