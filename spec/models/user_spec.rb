require 'rails_helper'

RSpec.describe User, type: :model do
  it "correct create user" do
    expect(FactoryBot.build(:user)).to be_valid
  end

  it "failed unless username" do
    user = FactoryBot.build(:user, username: nil)
    user.valid?

    expect(user.errors[:username]).to include("can't be blank")
  end

  it "failed unless email" do
    user = FactoryBot.build(:user, email: nil)
    user.valid?

    expect(user.errors[:email]).to include("can't be blank")
  end

  it "failed duplicate email" do
    FactoryBot.create(:user, email: "testzirou@example.com")
    user = FactoryBot.build(:user, email: "testzirou@example.com")
    user.valid?

    expect(user.errors[:email]).to include("has already been taken")
  end

  it "failed unless password" do
    user = FactoryBot.build(:user, password: nil)
    user.valid?

    expect(user.errors[:password]).to include("can't be blank")
  end
end
