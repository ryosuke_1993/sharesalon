require 'rails_helper'

RSpec.describe Request, type: :model do
  it "correct create request" do
    user = FactoryBot.create(:user)
    room = FactoryBot.create(:room)
    request = FactoryBot.build(:request, user_id: user.id, room_id: room.id)
    expect(request).to be_valid
  end

  it "failed past startdate" do
    user = FactoryBot.create(:user)
    room = FactoryBot.create(:room)
    request = FactoryBot.build(:request,
                               user_id: user.id, room_id: room.id, start_date: '2021-4-10 14:00:00', end_date: '2022-4-10 15:00:00',)
    request.valid?

    expect(request.errors[:start_date]).to include(":予約開始日は過去の日付を選択できません")
  end

  it "failed enddate before start_date" do
    user = FactoryBot.create(:user)
    room = FactoryBot.create(:room)
    request = FactoryBot.build(:request,
                               user_id: user.id,
                               room_id: room.id,
                               end_date: '2022-04-16 12:00:00')
    request.valid?

    expect(request.errors[:end_date]).to include(":予約終了時は開始時以前の日付を選択できません")
  end

  it "failed out of between open time" do
    user = FactoryBot.create(:user)
    room = FactoryBot.create(:room)
    request = FactoryBot.build(:request,
                               user_id: user.id,
                               room_id: room.id,
                               start_date: '2022-04-10 6:00:00',
                               end_date: '2022-4-10 7:00:00',)
    request.valid?
    expect(request.errors[:start_date]).to include("must be on or after 09:00:00")
  end

  it "failed tuesday" do
    user = FactoryBot.create(:user)
    room = FactoryBot.create(:room)
    request = FactoryBot.build(:request,
                               user_id: user.id,
                               room_id: room.id,
                               start_date: '2022-04-12 10:00:00',
                               end_date: '2022-4-12 11:00:00',)
    request.valid?
    expect(request.errors[:start_date]).to include(":予約開始日は火曜日を選択できません")
  end
end
