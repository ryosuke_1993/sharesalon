require 'rails_helper'

RSpec.describe Room, type: :model do
  it "correct create room" do
    expect(FactoryBot.build(:room)).to be_valid
  end
  it "failed unless title" do
    room = FactoryBot.build(:room, title: nil)
    room.valid?

    expect(room.errors[:title]).to include("can't be blank")
  end

  it "failed duplicate title" do
    FactoryBot.create(:room, title: "test")
    room = FactoryBot.build(:room, title: "test")
    room.valid?

    expect(room.errors[:title]).to include("has already been taken")
  end
end
