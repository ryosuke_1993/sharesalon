require 'rails_helper'

RSpec.describe Reservation, type: :model do
  it "correct create reservation" do
    user = FactoryBot.create(:user)
    room = FactoryBot.create(:room)
    reservation = FactoryBot.build(:reservation, user_id: user.id, room_id: room.id)
    expect(reservation).to be_valid
  end

  it "failed past startdate" do
    user = FactoryBot.create(:user)
    room = FactoryBot.create(:room)
    reservation = FactoryBot.build(:reservation,
                                   user_id: user.id, room_id: room.id, start_date: '2021-4-10 14:00:00', end_date: '2022-4-10 15:00:00',)
    reservation.valid?

    expect(reservation.errors[:start_date]).to include("は過去の日付を選択できません")
  end

  it "failed enddate before start_date" do
    user = FactoryBot.create(:user)
    room = FactoryBot.create(:room)
    reservation = FactoryBot.build(:reservation,
                                   user_id: user.id,
                                   room_id: room.id,
                                   end_date: '2022-04-16 12:00:00')
    reservation.valid?

    expect(reservation.errors[:end_date]).to include("は開始時以前の日付を選択できません")
  end

  it "failed out of between open time" do
    user = FactoryBot.create(:user)
    room = FactoryBot.create(:room)
    reservation = FactoryBot.build(:reservation,
                                   user_id: user.id,
                                   room_id: room.id,
                                   start_date: '2022-04-10 6:00:00',
                                   end_date: '2022-4-10 7:00:00',)
    reservation.valid?
    expect(reservation.errors[:start_date]).to include("must be on or after 09:00:00")
  end

  it "failed tuesday" do
    user = FactoryBot.create(:user)
    room = FactoryBot.create(:room)
    reservation = FactoryBot.build(:reservation,
                                   user_id: user.id,
                                   room_id: room.id,
                                   start_date: '2022-04-12 10:00:00',
                                   end_date: '2022-4-12 11:00:00',)
    reservation.valid?
    expect(reservation.errors[:start_date]).to include("は火曜日を選択できません")
  end

  it "failed duplicate reservation" do
    user = FactoryBot.create(:user)
    user2 = FactoryBot.create(:user)
    room = FactoryBot.create(:room)
    FactoryBot.create(:reservation, user_id: user.id, room_id: room.id)
    reserve_failed = FactoryBot.build(:reservation,
                                      user_id: user2.id,
                                      room_id: room.id,)
    reserve_failed.valid?
    expect(reserve_failed.errors[:start_date]).to include("は他のユーザーが予約しています")
  end
end
