Rails.application.routes.draw do

  root to: "home#index"
  devise_for :users
  devise_scope :user do
    post 'users/guest_sign_in', to: 'users/sessions#guest_sign_in'
  end
  resources :rooms do
    resources :reservations
  end
  resources :users do
    resources :requests
  end
  get 'admin', to: 'admin#index'
  namespace :admin do
    resources :rooms
    resources :users
    resources :reservations
  end
end
