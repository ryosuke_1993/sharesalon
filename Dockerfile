FROM ruby:2.6.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev
# yarnパッケージ管理ツールインストール
RUN apt-get update && apt-get install -y curl apt-transport-https wget && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update && apt-get install -y yarn

# Node.jsをインストール
RUN apt-get update -qq && apt-get install -y build-essential libxslt-dev libxml2-dev cmake
RUN apt-get install -y nodejs npm && npm install n -g && n 14.16.1
WORKDIR /saloshare
COPY Gemfile Gemfile.lock /saloshare/
RUN gem install bundler
RUN bundle install
